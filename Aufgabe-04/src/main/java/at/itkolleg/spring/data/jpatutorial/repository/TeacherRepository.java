package at.itkolleg.spring.data.jpatutorial.repository;

import at.itkolleg.spring.data.jpatutorial.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Long> {
}
