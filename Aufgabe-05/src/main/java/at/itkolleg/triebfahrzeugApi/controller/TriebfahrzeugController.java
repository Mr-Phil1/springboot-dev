package at.itkolleg.triebfahrzeugApi.controller;

import at.itkolleg.triebfahrzeugApi.entity.Triebfahrzeug;
import at.itkolleg.triebfahrzeugApi.error.TriebfahrzeugNotFoundException;
import at.itkolleg.triebfahrzeugApi.service.TriebfahrzeugService;
import at.itkolleg.triebfahrzeugApi.service.TriebfahrzeugServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TriebfahrzeugController {

    @Autowired
    TriebfahrzeugService triebfahrzeugService;

    @GetMapping("api/v1/triebfahrzeuge/")
    public List<Triebfahrzeug> gibTriebfahrzeuglist() {
        return this.triebfahrzeugService.gibAlleTriebfahrzeuge();
    }

    @GetMapping("api/v1/triebfahrzeuge/{id}")
    public Triebfahrzeug gibtriebfahrzeugMitId(@PathVariable("id") Long id) throws TriebfahrzeugNotFoundException {
        return this.triebfahrzeugService.gibTriebfahrzeugMitId(id);
    }

    @PostMapping("api/v1/triebfahrzeuge/")
    public Triebfahrzeug speicherTriebfahrzeug(Triebfahrzeug triebfahrzeug) {
        return this.triebfahrzeugService.triebfahrzeugEinfügen(triebfahrzeug);
    }
}
