package at.itkolleg.triebfahrzeugApi.entity;

        import lombok.AllArgsConstructor;
        import lombok.Builder;
        import lombok.Data;
        import lombok.NoArgsConstructor;

        import javax.persistence.Entity;
        import javax.persistence.GeneratedValue;
        import javax.persistence.GenerationType;
        import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Triebfahrzeug{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long triebfahrzeugId;

    private String baureihenNummer;
    private String herstellerName;
    private String herstellungsDatum;
    private String alternativNamen;
}
