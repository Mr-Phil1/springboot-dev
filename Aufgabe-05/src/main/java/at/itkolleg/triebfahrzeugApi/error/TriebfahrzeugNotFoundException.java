package at.itkolleg.triebfahrzeugApi.error;

public class TriebfahrzeugNotFoundException extends Exception{

    public TriebfahrzeugNotFoundException(String message) {
        super(message);
    }
}
