package at.itkolleg.triebfahrzeugApi.repository;

import at.itkolleg.triebfahrzeugApi.entity.Triebfahrzeug;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TriebfahrzeugRepository extends JpaRepository<Triebfahrzeug, Long> {

    Triebfahrzeug findAllByHerstellerName(String herstellerName);

    Triebfahrzeug findAllByAlternativNamen(String alternativNamen);
}
