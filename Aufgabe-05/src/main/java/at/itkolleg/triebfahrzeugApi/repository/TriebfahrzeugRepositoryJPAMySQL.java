package at.itkolleg.triebfahrzeugApi.repository;

import at.itkolleg.triebfahrzeugApi.entity.Triebfahrzeug;
import at.itkolleg.triebfahrzeugApi.error.TriebfahrzeugNotFoundException;
import at.itkolleg.triebfahrzeugApi.service.TriebfahrzeugDBZugriff;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;

public class TriebfahrzeugRepositoryJPAMySQL implements TriebfahrzeugDBZugriff {
    @Autowired
    private TriebfahrzeugRepository repository;

    @Override
    public Triebfahrzeug saveTriebfahrzeug(Triebfahrzeug triebfahrzeug) {
        return repository.save(triebfahrzeug);
    }

    @Override
    public Triebfahrzeug gibTriebfahrzeugMitId(Long id) throws TriebfahrzeugNotFoundException {
        Optional<Triebfahrzeug> triebfahrzeug = repository.findById(id);
        if (!triebfahrzeug.isPresent()) {
            throw new TriebfahrzeugNotFoundException("Triebfahrzeug Not Available");
        }

        return triebfahrzeug.get();
    }

    @Override
    public List<Triebfahrzeug> gibAlleTriebfahrzeuge() {
        return repository.findAll();
    }

    @Override
    public void deleteTriebfahrzeugMitId(Long id) {
        repository.deleteById(id);
    }



}
