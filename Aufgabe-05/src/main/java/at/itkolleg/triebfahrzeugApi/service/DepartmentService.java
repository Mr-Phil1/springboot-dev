package at.itkolleg.triebfahrzeugApi.service;

import at.itkolleg.triebfahrzeugApi.entity.Department;
import at.itkolleg.triebfahrzeugApi.error.DepartmentNotFoundException;

import java.util.List;

public interface DepartmentService {
    public Department saveDepartment(Department department);

    public List<Department> fetchDepartmentList();

    public Department fetchDepartmentById(Long departmentID) throws DepartmentNotFoundException;

   public void deleteDepartmentById(Long departmentID);

   public Department updateDepartment(Long departmentID, Department department);

   public Department fetchDepartmentByName(String departmentName);
}
