package at.itkolleg.triebfahrzeugApi.service;

import at.itkolleg.triebfahrzeugApi.entity.Triebfahrzeug;
import at.itkolleg.triebfahrzeugApi.error.TriebfahrzeugNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface TriebfahrzeugDBZugriff {


    Triebfahrzeug saveTriebfahrzeug(Triebfahrzeug triebfahrzeug);

    Triebfahrzeug gibTriebfahrzeugMitId(Long id) throws TriebfahrzeugNotFoundException;

    List<Triebfahrzeug> gibAlleTriebfahrzeuge();

    void deleteTriebfahrzeugMitId(Long id);

}
