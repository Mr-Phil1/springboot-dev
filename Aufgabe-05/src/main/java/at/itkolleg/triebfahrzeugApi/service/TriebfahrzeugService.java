package at.itkolleg.triebfahrzeugApi.service;

import at.itkolleg.triebfahrzeugApi.entity.Triebfahrzeug;
import at.itkolleg.triebfahrzeugApi.error.TriebfahrzeugNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface TriebfahrzeugService {

    List<Triebfahrzeug> gibAlleTriebfahrzeuge();

    Triebfahrzeug triebfahrzeugEinfügen(Triebfahrzeug triebfahrzeug);

    void triebfahrzeugLöschenMitId(Long id);

    Triebfahrzeug gibTriebfahrzeugMitId(Long id) throws TriebfahrzeugNotFoundException;
}
