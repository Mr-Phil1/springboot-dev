package at.itkolleg.triebfahrzeugApi.service;

import at.itkolleg.triebfahrzeugApi.entity.Triebfahrzeug;
import at.itkolleg.triebfahrzeugApi.error.TriebfahrzeugNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class TriebfahrzeugServiceImpl implements TriebfahrzeugService {

    @Autowired
    private TriebfahrzeugDBZugriff dbZugriff;

    public TriebfahrzeugServiceImpl(TriebfahrzeugDBZugriff dbZugriff) {
        this.dbZugriff = dbZugriff;
    }

    @Override
    public List<Triebfahrzeug> gibAlleTriebfahrzeuge() {
        return this.dbZugriff.gibAlleTriebfahrzeuge();
    }

    @Override
    public Triebfahrzeug triebfahrzeugEinfügen(Triebfahrzeug triebfahrzeug) {
        return this.dbZugriff.saveTriebfahrzeug(triebfahrzeug);
    }

    @Override
    public void triebfahrzeugLöschenMitId(Long id) {
        this.dbZugriff.deleteTriebfahrzeugMitId(id);
    }

    @Override
    public Triebfahrzeug gibTriebfahrzeugMitId(Long id) throws TriebfahrzeugNotFoundException {
        return this.dbZugriff.gibTriebfahrzeugMitId(id);
    }


}
