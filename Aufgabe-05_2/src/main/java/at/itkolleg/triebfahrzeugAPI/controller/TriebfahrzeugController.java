package at.itkolleg.triebfahrzeugAPI.controller;

import at.itkolleg.triebfahrzeugAPI.entity.Triebfahrzeug;
import at.itkolleg.triebfahrzeugAPI.error.TriebfahrzeugNotFoundException;
import at.itkolleg.triebfahrzeugAPI.service.TriebfahrzeugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TriebfahrzeugController {
    @Autowired
    private TriebfahrzeugService triebfahrzeugService;

    @GetMapping("api/v1/triebfahrzeuge/")
    public List<Triebfahrzeug> gibTriebfahrzeuglist() {
        return triebfahrzeugService.gibAlleTriebfahrzeuge();
    }

    @GetMapping("api/v1/triebfahrzeuge/id/{id}")
    public Triebfahrzeug gibTriebfahrzeugMitId(@PathVariable("id") Long id) throws TriebfahrzeugNotFoundException {
        return triebfahrzeugService.gibTriebfahrzeugMitId(id);
    }

    @PostMapping("api/v1/triebfahrzeuge/")
    public Triebfahrzeug speicherTriebfahrzeug(@RequestBody Triebfahrzeug triebfahrzeug) {
        return triebfahrzeugService.speicherTriebfahrzeug(triebfahrzeug);
    }

    @GetMapping("api/v1/triebfahrzeuge/name/{alternative}")
    public List<Triebfahrzeug> gibAlleTriebfahrzeugeMitAlternativeNamen(@PathVariable("alternative") String alternativeNamen) throws TriebfahrzeugNotFoundException {
        return triebfahrzeugService.gibAlleTriebfahrzeugeAlternativeNamen(alternativeNamen);
    }

    @GetMapping("api/v1/triebfahrzeuge/vendor/{hersteller}")
    public List<Triebfahrzeug> gibAlleTriebfahrzeugeMitHerstellerNamen(@PathVariable("hersteller") String herstelllerNamen) throws TriebfahrzeugNotFoundException {
        return triebfahrzeugService.gibAlleTriebfahrzeugHerstellerNamen(herstelllerNamen);
    }
}
