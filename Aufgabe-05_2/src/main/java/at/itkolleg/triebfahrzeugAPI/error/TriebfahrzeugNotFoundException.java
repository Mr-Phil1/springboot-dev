package at.itkolleg.triebfahrzeugAPI.error;

public class TriebfahrzeugNotFoundException extends Exception{
    public TriebfahrzeugNotFoundException(String message) {
        super(message);
    }
}
