package at.itkolleg.triebfahrzeugAPI.repository;

import at.itkolleg.triebfahrzeugAPI.entity.Triebfahrzeug;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TriebfahrzeugRepository extends JpaRepository<Triebfahrzeug, Long> {
    List<Triebfahrzeug> findAllByAlternativNamenIgnoreCase(String alternativeName);

    List<Triebfahrzeug>  findAllByHerstellerNameIgnoreCase(String herstellerName);
}
