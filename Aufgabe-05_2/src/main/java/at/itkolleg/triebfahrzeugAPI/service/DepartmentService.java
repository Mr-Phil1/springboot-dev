package at.itkolleg.triebfahrzeugAPI.service;

import at.itkolleg.triebfahrzeugAPI.entity.Department;
import at.itkolleg.triebfahrzeugAPI.error.DepartmentNotFoundException;

import java.util.List;

public interface DepartmentService {
    public Department saveDepartment(Department department);

    public List<Department> fetchDepartmentList();

    public Department fetchDepartmentById(Long departmentID) throws DepartmentNotFoundException;

   public void deleteDepartmentById(Long departmentID);

   public Department updateDepartment(Long departmentID, Department department);

   public Department fetchDepartmentByName(String departmentName);
}
