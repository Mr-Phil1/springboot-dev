package at.itkolleg.triebfahrzeugAPI.service;

import at.itkolleg.triebfahrzeugAPI.entity.Triebfahrzeug;
import at.itkolleg.triebfahrzeugAPI.error.TriebfahrzeugNotFoundException;

import java.util.List;

public interface TriebfahrzeugService {
    Triebfahrzeug speicherTriebfahrzeug(Triebfahrzeug triebfahrzeug);

    List<Triebfahrzeug> gibAlleTriebfahrzeuge();

    Triebfahrzeug gibTriebfahrzeugMitId(Long id) throws TriebfahrzeugNotFoundException;

    void löscheTriebfahrzeugMitId(Long id);

    List<Triebfahrzeug>  gibAlleTriebfahrzeugHerstellerNamen(String herstellerNamen) throws TriebfahrzeugNotFoundException;

    List<Triebfahrzeug>  gibAlleTriebfahrzeugeAlternativeNamen(String alternativeNamen) throws TriebfahrzeugNotFoundException;
}
