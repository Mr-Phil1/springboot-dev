package at.itkolleg.triebfahrzeugAPI.service;

import at.itkolleg.triebfahrzeugAPI.entity.Triebfahrzeug;
import at.itkolleg.triebfahrzeugAPI.error.TriebfahrzeugNotFoundException;
import at.itkolleg.triebfahrzeugAPI.repository.TriebfahrzeugRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TriebfahrzeugServiceImpl implements TriebfahrzeugService {
    @Autowired
    private TriebfahrzeugRepository triebfahrzeugRepository;

    @Override
    public Triebfahrzeug speicherTriebfahrzeug(Triebfahrzeug triebfahrzeug) {
        return triebfahrzeugRepository.save(triebfahrzeug);
    }

    @Override
    public List<Triebfahrzeug> gibAlleTriebfahrzeuge() {
        return triebfahrzeugRepository.findAll();
    }

    @Override
    public Triebfahrzeug gibTriebfahrzeugMitId(Long id) throws TriebfahrzeugNotFoundException {
        Optional<Triebfahrzeug> triebfahrzeug = triebfahrzeugRepository.findById(id);
        if (!triebfahrzeug.isPresent()) {
            throw new TriebfahrzeugNotFoundException("Triebfahrzeug not Available");
        }
        return triebfahrzeug.get();
    }

    @Override
    public void löscheTriebfahrzeugMitId(Long id) {
        triebfahrzeugRepository.deleteById(id);
    }

    @Override
    public List<Triebfahrzeug> gibAlleTriebfahrzeugHerstellerNamen(String herstellerNamen) throws TriebfahrzeugNotFoundException {
        List<Triebfahrzeug> list = triebfahrzeugRepository.findAllByHerstellerNameIgnoreCase(herstellerNamen);
        if (list.isEmpty()) {
            throw new TriebfahrzeugNotFoundException("");
        }
        return list;
    }

    @Override
    public List<Triebfahrzeug> gibAlleTriebfahrzeugeAlternativeNamen(String alternativeNamen) throws TriebfahrzeugNotFoundException {
        List<Triebfahrzeug> list = triebfahrzeugRepository.findAllByAlternativNamenIgnoreCase(alternativeNamen);
        if (list.isEmpty()) {
            throw new TriebfahrzeugNotFoundException("");
        }
        return list;

    }
}
