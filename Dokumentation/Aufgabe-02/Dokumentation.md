---
title: Dokumentation
subtitle: MyUserManager
author: Mr. Phil
rights: Nah
language: de-AT
keywords: POS-FSE; JAVA; SpringBoot
titlepage: true
titlepage-color: "F9C48C"
titlepage-text-color: "FFFFFF"
table-use-row-colors: true
toc-own-page: true
---
# Einleitung



# UML-Diagramm des Projektes

![Projekt UML-Diagramm](Bilder/Diagram1.png)

# Projekt Mock-Up

![index.html](Bilder/03_index-html.png)

![users.html](Bilder/14_manage-user_table.png)

![users_form.html](Bilder/16_Add-new-User_form-finish.png)


# Verwendete Technologien
| Technologien-Name           | Verwendete Version |
| --------------------------- | ------------------ |
| Java                        | 11                 |
| Spring Boot Framework       | 2.5.5              |
| Spring Data JPA & Hibernate | 2.5.5              |
| MySQL Database              | 8.0.23             |
| mysql-connector-java        | 8.0.26             |
| Thymeleaf                   | 2.5.5              |
| Bootstrap CSS (org.webjars) | 4.3.1              |
| Unit 5 & AssertJ            |
| IntelliJ Ultimate           | 2021.2.3           |


\pagebreak
# Initialisierung

## Projekt Erzeugung
Für die Erzeugung dieses Projektes gibt es zwei Möglichkeiten meiner Meinung nach. Einerseits kann man das Maven Projekt über die IntelliJ-Ultimate Oberläche oder über die SpringBoot Start Webseite erzeugen.

### Möglichkeit 1 (IntelliJ)

Diese Möglichkeit ist nur machbar wenn man die Ultimate Edition von IntelliJ in Verwendung hat. Sollte es der Fall sein dass man diese Edition nicht zur Hand haben, so empfehle ich sofort zur **[Möglichkeit 2](#möglichkeit-2-springboot-web-init)** zuspringen.

![IntelliJ: Neues Spring Projekt](Bilder/23_new-project_intelliJ_01.png){height=300px width=300px}

![Hinzufügen der jeweiligen Dependencies](Bilder/24_new-project_intelliJ_02.png){height=300px width=300px}

\pagebreak
### Möglichkeit 2 (SpringBoot Web Init)
In dem Quellenverzeichnis habe ich einmal die Spring Start Webseite ohne bzw. mit den verwendeten Dependencies verlinkt. Siehe: [Springboot-Web](#springboot-start)

![Ansicht: spring init mit den ausgewählten Dependencies](Bilder/25_new-project_spring-init_01.png){height=550px width=550px}

![Erste Übersicht der pom.xml Datei](Bilder/26_new-project_spring-init_02.png){height=550px width=550px}

![Die pom.xml auswählen](Bilder/27_new-project_spring-init_03.png){height=300px width=350px}

![Den Button "Open as Project" klicken](Bilder/28_new-project_spring-init_04.png){height=300px width=350px}

\pagebreak
### Ordnerstruktur
In dem folgendem Bild kann man gut die Ordnerstruktur des zuvor importierten bzw. erstellten Maven Projektes erkennen. 

![Ordnerstruktur IntelliJ](Bilder/29_Ordnerstruktur.png)
  
* **java-Ordner**
  * In diesem Ordner werden sämtliche Java-Klassen erstellt und geschrieben
* **resources-Ordner**
  * Hier werden einmal die Thymleaf HTML-Templates erstellt
  * Weiters wird in der application.properties Datei die individuale SpringBoot Konfiguration definiert 
    * Siehe: [Benützte Config](#konfiguration)
* **test-Ordner**
  * Sollte man JUnit-Test erstellen, so sollten diese in diesem Ordner erstellt werden

\pagebreak
## Dependencies in der pom.xml

### Springboot
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-thymeleaf</artifactId>
</dependency>

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-web</artifactId>
</dependency>

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-devtools</artifactId>
    <scope>runtime</scope>
    <optional>true</optional>
</dependency>
```

### MySQL 
```xml
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <scope>runtime</scope>
</dependency>
```

### Bootstrap 
```xml
<dependency>
    <groupId>org.webjars</groupId>
    <artifactId>bootstrap</artifactId>
    <version>4.3.1</version>
</dependency>

<dependency>
    <groupId>org.webjars</groupId>
    <artifactId>webjars-locator-core</artifactId>
</dependency>
```

## MySQL Datenbank Erstellung

### Möglichkeit 1 (CLI)

```sql
create schema usersdb collate utf8mb4_general_ci;
```

### Möglichkeit 2 (IntelliJ GUI)

![usersdb Erzeugen mit Hilfe von IntellJ](Bilder/30_datenbanktabelle-erzeugen.png){height=300px width=350px}

## Konfiguration

```yml
server.port=9876
spring.datasource.url=jdbc:mysql://localhost:3306/usersdb
spring.datasource.username=root
spring.datasource.password=123
spring.jpa.hibernate.ddl-auto=update
spring.jpa.properties.hiernate.show_sql=true
```

Bei dieser Konfiguration wird zunächst der SpringBoot Server Port vom Standartwert 8080 auf den Wert 9876 gesetzt. Weiters wird SpringBoot gesagt wo die MySQL Datenbank zufinden ist und mit welchem Treiber SpringBoot diese Datenbank ansprechen soll. Die nächsten zwei Zeilen geben den Benutzer und das Passwort der MySQL Datenbank an. **Hinweis: Das es sich hierbei um eine temporäre MySQL-Instanz handelt wurde hierbei mit dem User root und dem Password 123 gearbeitet. Ich rate hierbei ab dass man denn User Root und ein so schwaches Passwort verwendet, und weiters sollte man sollche Daten niemals in die Öffentlichkeit geben.**

\pagebreak
# MainController
Dieser Controller hat die Zuständigkeit damit das [index.html Template](#resources---templates) angezeigt wird.
```java
@GetMapping("")
public String showHomePage(){
   System.out.println("main controller");
   return "index";
}
```

\pagebreak
# User-Package
## User
### Datenfelder
Mit diesen Datenfelder wird in der zuvor erstellten usersdb ([siehe: MySQL-Datenbank Erstellung](#mysql-datenbank-erstellung)) Datenbank, die Tabelle users generiert. Zunächst wird definert das Datenfeld *id* der PrimaryKey wird. Weiters werden die Felder *email*, *password*, *fist_name*, *last_name* und *enabled* erstellt.


```java
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer id;
@Column(nullable = false, unique = true, length = 45)
private String email;

@Column(length = 15, nullable = false)
private String password;

@Column(length = 45, nullable = false,name="first_name")
private String firstName;

@Column(length = 15, nullable = false,name="last_name")
private String lastName;

private boolean enabled;    
```

* Infos über die jeweiligen Felder (alle Felder bis auf enabled dürfen **nicht NULL** sein):
  * **email**
    * darf höchstens 45 Zeichen lang sein
    * muss einmalig sein
  * **password**
    * darf höchstens 15 Zeichen lang sein
  * **firstName**
    * darf höchstens 45 Zeichen lang sein
    * hat zusätzlich den Feldnamen first_name
  * **lastName**
    * darf höchstens 15 Zeichen lang sein
    * hat zusätzlich den Feldnamen last_name
  * **enabled**
    * bekommt einen boolen wert

![Erstellte users Tabelle](Bilder/04_created-user_tables.png)

\pagebreak
### Getter + Setter
```java
public Integer getId() {
    return id;
}

public void setId(Integer id) {
    this.id = id;
}

public String getEmail() {
    return email;
}

public void setEmail(String email) {
    this.email = email;
}

public String getPassword() {
    return password;
}

public void setPassword(String password) {
    this.password = password;
}

public String getFirstName() {
    return firstName;
}

public void setFirstName(String firstName) {
    this.firstName = firstName;
}

public String getLastName() {
    return lastName;
}

public void setLastName(String lastName) {
    this.lastName = lastName;
}

public boolean isEnabled() {
    return enabled;
}

public void setEnabled(boolean enabled) {
    this.enabled = enabled;
}
```
### toString-Methode
```java
@Override
public String toString() {
    return "User{" +
            "id=" + id +
            ", email='" + email + '\'' +
            ", password='" + password + '\'' +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            '}';
}
```
## UserController
```java
@Autowired
private UserService service;
```

Die Annotation @Autowired wird hier beschrieben: **[Autowired](#autowired)**

### showUserList
```java
@GetMapping("/users")
public String showUserList(Model model) {
    List<User> listUsers = service.listAll();
    model.addAttribute("listUsers", listUsers);
    return "users";
}
```

### showNewForm
```java
@GetMapping("/users/new")
public String showNewForm(Model model) {
    model.addAttribute("user", new User());
    model.addAttribute("pageTitle", "Add New User");
    return "user_form";
}
```

### showEditForm
```java
@GetMapping("/users/edit/{id}")
public String showEditForm(@PathVariable("id") Integer id, Model model, RedirectAttributes ra) {
    try {
        User user = service.get(id);
        model.addAttribute("user", user);
        model.addAttribute("pageTitle", "Edit User (ID: " + id + ")");
        return "user_form";
    } catch (UserNotFoundException e) {
        ra.addFlashAttribute("message", e.getMessage());
        return "redirect:/users";
    }
}
```

### deleteUser
```java
@GetMapping("/users/delete/{id}")
public String deleteUser(@PathVariable("id") Integer id, RedirectAttributes ra) {
    try {
        service.delete(id);
        ra.addFlashAttribute("message", "The user ID " + id + " has been deleted.");
    } catch (UserNotFoundException e) {
        ra.addFlashAttribute("message", e.getMessage());
    }
    return "redirect:/users";
}
```

### saveUser
```java
@PostMapping("/users/save")
public String saveUser(User user, RedirectAttributes ra) {
    service.save(user);
    ra.addFlashAttribute("message", "The user has been saved successfully.");
    return "redirect:/users";
}
```

\pagebreak
## UserNotFoundException
```java
public class UserNotFoundException extends Throwable {
    public UserNotFoundException(String message) {
        super(message);
    }
}

```
## UserRepository
```java
public interface UserRepository extends CrudRepository<User, Integer> {
    public Long countById(Integer id);
}
```

\pagebreak
## UserService
```java
    @Autowired
    private UserRepository repo;
```

Die Annotation @Autowired wird hier beschrieben: **[Autowired](#autowired)**

### listAll
```java
 public List<User> listAll() {
        return (List<User>) repo.findAll();
    }
```

### save
```java
    public void save(User user) {
        repo.save(user);
    }
```

### get
```java
public User get(Integer id) throws UserNotFoundException {
        Optional<User> result = repo.findById(id);
        if (result.isPresent()) {
            return result.get();
        }
        throw new UserNotFoundException("Could not find any users with ID " + id);
    }
```

### delete
```java
   public void delete(Integer id) throws UserNotFoundException {
        Long count = repo.countById(id);
        if (count == null || count == 0) {
            throw new UserNotFoundException("Could not find any users with ID " + id);
        }
        repo.deleteById(id);
    }
```

\pagebreak
# resources -> templates
## index.html

![Ansicht: index.html](Bilder/03_index-html.png)

## user_form.html

![Ansicht: Add new User Form](Bilder/16_Add-new-User_form-finish.png){height=300px width=350px}


## users.html

![Ansicht: User Table](Bilder/14_manage-user_table.png)

\pagebreak
# JUnit-Tests
## UserRepositoryTests

```java
@Autowired
private UserRepository repo;
```

Die Annotation @Autowired wird hier beschrieben: **[Autowired](#autowired)**

### testAddNew
Dieser Test erezugt einen neuen User mit dem Namen (Alex Steveson), der E-Mail Addr. (alex.steveson@gmail.com) und dem Passwort (alex123456). Nachdem der neue User im UserRepository gespeichert wurde, wird überprüft ob dieser User nicht NULL ist, sprich ob dieser User vorhanden ist, und ob die ID von diesem User größer als 0 ist.
```java
@Test
public void testAddNew() {
    User user = new User();
    user.setEmail("alex.steveson@gmail.com");
    user.setPassword("alex123456");
    user.setFirstName("Alex");
    user.setLastName("Steveson");

    User savedUser = repo.save(user);

    Assertions.assertThat(savedUser).isNotNull();
    Assertions.assertThat(savedUser.getId()).isGreaterThan(0);
}
```

### testListAll
Dieser Test gibt sämtliche User aus der Datenbank auf die Ausgabe aus, nachdem überprüft worden ist ob User vorhanden sind.

```java
@Test
public void testListAll() {
    Iterable<User> users = repo.findAll();
    Assertions.assertThat(users).hasSizeGreaterThan(0);

    for (User user : users) {
        System.out.println(user);
    }
    }
```

### testUpdate
Bei diesem Test wird Zunächst ein User mit einer gewissen ID gesucht und zwischengespeichert. Im Anschluss wird bei diesem zwischengespeichertem User das Passwort geändert, nach dieser Änderung wird dieser User wieder in die Datenbank gespeichert. Danach wird bei dem neuen User in der Datenbank überprüft ob die Passwörter Übereinstimmen.

```java
@Test
public void testUpdate() {
    Integer userId = 1;
    Optional<User> optionalUser = repo.findById(userId);
    User user = optionalUser.get();
    user.setPassword("hello2000");
    repo.save(user);

    User updatedUser = repo.findById(userId).get();
    Assertions.assertThat(updatedUser.getPassword()).isEqualTo("hello2000");
}
```

### testGet
Hier bei wird geprüft ob das Holen eines User mit einer gewissen ID aus der Datenbank funktioniert.
```java
@Test
public void testGet() {
    Integer userId = 2;
    Optional<User> optionalUser = repo.findById(userId);
    Assertions.assertThat(optionalUser).isPresent();
    System.out.println(optionalUser.get());
}
```

### testDelete
Dieser Test löscht zunächt einen User mit einer gewissen ID, im Anschluss wird in der Datenbank nach einen User mit dieser ID gesucht und danach wird geprüt ob dieser User nicht vorhanden ist.

```java
@Test
public void testDelete() {
    Integer userId = 2;
    repo.deleteById(userId);
    Optional<User> optionalUser = repo.findById(userId);
    Assertions.assertThat(optionalUser).isNotPresent();
}
```

\pagebreak
# SpringBoot Annotation
## @Autowired
Diese Annotation bewirkt dass SpringBoot zur Laufzeit nach sämtlichen Implementationen eines Interfaces sucht, sollte dann eine passente Implementation gefunden sein so wird dann eine Objekt-Instanz davon in die definierte Variable gespeichert. Dadurch erspart man sich die eindeutige Deklaration der gewünschten Implementation für dieses Interface, weiters bewirkt dieser Zusatz auch das Problem der Abhänigkeiten von Objekten zur Laufzeit.

## @Mapping
Usprünglich hatte Spring nur die `@RequestMapping` Annotation für das Verarbeiten sämtlicher HTTP-Anfragen. Mit der Spring Version **4.3** wurden folgende Annotationen eingeführt:

* `@GetMapping`
* `@PostMapping`
* `@PutMapping`
* `@DeleteMapping`
* `@PatchMapping`
 
### @GetMapping
Diese Springboot Annotation ist für sämtliche HTTP-GET-Anfragen zuständdig.

* Alternative Schrweibweise
```java
 @RequestMapping(method = RequestMethod.GET).
```
### @PostMapping
Diese Springboot Annotation ist für sämtliche HTTP-POST-Anfragen zuständdig.

* Alternative Schrweibweise
```java
 @RequestMapping(method = RequestMethod.POST).
```


\pagebreak
# Literatur und Quellenverzeichnis
## Literaturverzeichnis
### Youtube-Video:
  * [Spring Boot CRUD Tutorial with IntelliJ IDEA, MySQL, JPA, Hibernate, Thymeleaf and Bootstrap](https://www.youtube.com/watch?v=u8a25mQcMOI) [Abgerufen: 2021-10-27, Ersteller: Code Java]

## Quellenverzeichnis
### SpringBoot Start:
  * [spring initializr](https://start.spring.io/)
  * [spring initializr mit den genützten Dependencies](https://start.spring.io/#!dependencies=thymeleaf,devtools,web,data-jpa,mysql)

## Abbildungsverzeichnis
\renewcommand{\listfigurename}{}
\listoffigures