---
title: Dokumentation
subtitle: DepartmentManager
author: Mr. Phil
rights: Nah
language: de-AT
keywords: POS-FSE; JAVA; SpringBoot
titlepage: true
titlepage-color: "6B8790"
titlepage-text-color: "FFFFFF"
table-use-row-colors: true
toc-own-page: true
---

# Verwendete Technologien

| Technologien-Name           | Verwendete Version |
| --------------------------- | ------------------ |
| Java                        | 11                 |
| Spring Boot Framework       | 2.5.5              |
| Spring Data JPA & Hibernate | 2.5.5              |
| MySQL Database              | 8.0.23             |
| mysql-connector-java        | 8.0.26             |
| Unit 5 & AssertJ            | 5.7.2              |
| IntelliJ Ultimate           | 2021.2.3           |
| Project Lombok              | 1.18.20            |

\pagebreak

# Bilder

![MainController: Hello World](Bilder/01-helloWorldController-Welcome.png)

![Maven SpringBoot run: CLI](Bilder/02-start-springboot-terminal.png)

![Project Auto-Build: Enable](Bilder/03-enable-auto-build.png)

![h2-Console: login](Bilder/04-h2_database-web-console.png)

![h2-Console: startpage](Bilder/05-h2_database-web-console.png)

![h2-Console: department-Table](Bilder/06-h2_database-department_table.png)

![Insomnia: save department](Bilder/07-Insomnia-save_Department.png)

![h2-Console: select department](Bilder/08-h2_database-department_select.png)

![Insomnia: get department](Bilder/09-Insomnia-get_Department.png)

![Insomnia: get department mit ID](Bilder/10-Insomnia-get_Department-by-id.png)

![Insomnia: delete department mit ID](Bilder/11-Insomnia-delete_Department-by-id.png)

![Insomnia: update department mit ID](Bilder/12-Insomnia-update_Department-by-id.png)

![h2-Console: select department (mit denn Updates)](Bilder/13-h2_database-department_update.png)

![Insomnia: Ausgabe Department mit bestimmten Namen (CE)](Bilder/14-Insomnia-get_Department-by-name.png)

![Insomnia: Fehlermeldung bei der Speicherung eines Departments ohne Namen](Bilder/15-Insomnia-save_Department-with-out-name.png)

![IntelliJ: Ausgabe Logger saveDepartment](Bilder/16-IntelliJ_console-Logger_saveDepartment.png)

![IntelliJ: Ausgabe Logger fetchDepartment](Bilder/17-IntelliJ_console-Logger_fetchDepartmentList.png)

![Insomnia: Fehlermeldung "Department Not Available full"](Bilder/18-Insomnia-get_Department-by-id_exception-message.png)

![Insomnia: Fehlermeldung "Department Not Available"](Bilder/19-Insomnia-get_Department-by-id_Not-Found+Message.png)

![MySQL-Workbench: select department](Bilder/20-MySQL_Workbench-create_dcapp-table+select_*.png)

![IntelliJ: Test passed (whenValidDepartmentName)](Bilder/21-ValidDepartment-shouldFound_test-passed.png)

![IntelliJ: Test passed (whenValidDepartmentName) mit geänderten Titel](Bilder/22-ValidDepartment-shouldFound_test-passed-with-a-displayName.png)

![IntelliJ: Test passed (whenindById_thenReturnDepartment)](Bilder/23_whenFindById_thenReturnDepartment_test-passed.png)

![IntelliJ: Test passed (saveDepartment)](Bilder/24_saveDepartment_test-passed.png)

![IntelliJ: Test passed (fetchDepartmentById)](Bilder/25_fetchDepartmentById_test-passed.png)

![IntelliJ: Profil geändert auf "qa"](Bilder/26_springBoot-profileChange.png)

![IntelliJ: SpringBoot Anwendung auf System installiert + .jar File erstellt](Bilder/27_mvn-clean-install_jar.png)

![CLI: Erstellte .jar File mit dem Profil "prod" gestartet](Bilder/28_start-created-jar_with-the-prod-profile.png)

![Firefox: Actuator Ausgabe mit den minimalen Endpoints](Bilder/29_springBoot-actuator_endpoint.png)

![Firefox: Actuator Ausgabe mit allen Entpoints](Bilder/30_springBoot-actuator_endpoint-all_enabled.png)

![Firefox: Actuator Ausgabe mit benutzerdefinierten Endpoints](Bilder/31_springBoot-actuator_endpoint-create_features-endpoint.png)

# Project Lombok Annotationen

- `@Data`
- `@NoArgsConstructor`
- `@AllArgsConstructor`
- `@Builder`

## Data

## NoArgsConstructor

## AllArgsConstructor

## Builder

\pagebreak

# Literatur- und Quellenverzeichnis

## Literaturverzeichnis

### Youtube-Video:

- [Spring Boot Tutorial | Full In-depth Course](https://www.youtube.com/watch?v=c3gKseNAs9w) [Abgerufen: 2021-11-03, Ersteller: Daily Code Buffer]

## Quellenverzeichnis

### SpringBoot Start:

- [spring initializr](https://start.spring.io/)
- [spring initializr mit den genützten Dependencies](https://start.spring.io/#!dependencies=mysql,web,h2,devtools,data-jpa,actuator,validation,lombok)

## Abbildungsverzeichnis

\renewcommand{\listfigurename}{}
\listoffigures
