---
title: Dokumentation
subtitle: StudentManager
author: Mr. Phil
rights: Nah
language: de-AT
keywords: POS-FSE; JAVA; SpringBoot
titlepage: true
titlepage-color: "6B8790"
titlepage-text-color: "FFFFFF"
table-use-row-colors: true
toc-own-page: true
---

# Verwendete Technologien

| Technologien-Name           | Verwendete Version |
| --------------------------- | ------------------ |
| Java                        | 11                 |
| Spring Boot Framework       | 2.5.5              |
| Spring Data JPA & Hibernate | 2.5.5              |
| MySQL Database              | 8.0.23             |
| mysql-connector-java        | 8.0.26             |
| Unit 5 & AssertJ            | 5.7.2              |
| IntelliJ Ultimate           | 2021.2.3           |
| Project Lombok              | 1.18.20            |

\pagebreak

# Bilder

![](Bilder/01_create-schooldb.png)

![](Bilder/02_create-tbl_student.png)

![](Bilder/03_saveStudent_test-passed.png)

![](Bilder/04_created-testUser.png)

![](Bilder/05_printAllStudent_test-passed_listAllStudent.png)

![](Bilder/06_saveStudentWithGuardian_test-passed.png)

![](Bilder/07_created-testUser2.png)

![](Bilder/08_printStudentByFirstName_test-passed.png)

![](Bilder/09_printStudentByFirstNameContaining_test-passed.png)

![](Bilder/10_printStudentBasedOnGuardianName_test-passed.png)

![](Bilder/11_printgetStudentByEmailAddress_test-passed.png)

![](Bilder/12_printgetStudentFirstNameByEmailAddress_test-passed.png)

![](Bilder/13_printgetStudentByEmailAddressNative_test-passed.png)

![](Bilder/14_getStudentByEmailAddressNativeNamedParam.png)

![](Bilder/15_updateStudentNameByEmailId_test-passed.png)

![](Bilder/16_updateStudentNameByEmailId_updated-firstName.png)

![](Bilder/17_create_course-and-courseMaterial_Table.png)

![](Bilder/18_create_course-and-courseMaterial_Table.png)

![](Bilder/19_saveCourseMaterial_test-failed.png)

![](Bilder/20_saveCourseMaterial_test-passed.png)

![](Bilder/21_save-courseMaterial.png)

![](Bilder/22_save-course.png)

![](Bilder/23_printAllCourseMaterials_test-failed.png)

![](Bilder/24_printAllCourseMaterials_test-passed.png)

![](Bilder/25_printCourses_test-passed.png)

![](Bilder/26_printCourses_test-passed.png)

![](Bilder/27_create_teacher-table.png)

![](Bilder/28_create_teacher-table.png)

![](Bilder/29_saveTeacher_test-passed.png)

![](Bilder/30_created-teacher.png)

![](Bilder/31_created-courses.png)

![](Bilder/32_saveCourseWithTeacher_course+teacher-in-db.png)

![](Bilder/32_saveCourseWithTeacher_test-passed.png)

![](Bilder/33_findAllPagination-with1Page+3Records_test-passed.png)

![](Bilder/34_findAllPagination-with2Page+2Records_test-passed.png)

![](Bilder/35_findAllSorting_test-passed.png)

![](Bilder/36_printFindByTitleContaining_test-passed.png)

![](Bilder/37_create-student_course_map-table.png)

![](Bilder/38_create-student_course_map-table.png)

![](Bilder/39_saveCourseWithStudentAndTeacher_test-failed.png)

![](Bilder/40_saveCourseWithStudentAndTeacher_test-passed.png)

![](Bilder/41_inserted-data-in-the-db.png)

# Project Lombok Annotationen

- `@Data`
- `@NoArgsConstructor`
- `@AllArgsConstructor`
- `@Builder`

\pagebreak

# Literatur- und Quellenverzeichnis

## Literaturverzeichnis

### Youtube-Video:

- [Spring Data JPA Tutorial | Full In-depth Course](https://www.youtube.com/watch?v=XszpXoII9Sg) [Abgerufen: 2021-11-06, Ersteller: Daily Code Buffer]

## Quellenverzeichnis

### SpringBoot Start:

- [spring initializr](https://start.spring.io/)
- [spring initializr mit den genützten Dependencies](https://start.spring.io/#!dependencies=mysql,web,h2,devtools,data-jpa,actuator,validation,lombok)

## Abbildungsverzeichnis

\renewcommand{\listfigurename}{}
\listoffigures
